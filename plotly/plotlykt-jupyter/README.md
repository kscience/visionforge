# Module plotlykt-jupyter



## Usage

## Artifact:

The Maven coordinates of this project are `space.kscience:plotlykt-jupyter:0.7.2`.

**Gradle Kotlin DSL:**
```kotlin
repositories {
    maven("https://repo.kotlin.link")
    mavenCentral()
}

dependencies {
    implementation("space.kscience:plotlykt-jupyter:0.7.2")
}
```
