rootProject.name = "visionforge"

enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")

pluginManagement {

    val toolsVersion: String by extra

    repositories {
        mavenLocal()
        maven("https://repo.kotlin.link")
        mavenCentral()
        gradlePluginPortal()
        maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
    }

    plugins {
        id("space.kscience.gradle.project") version toolsVersion
        id("space.kscience.gradle.mpp") version toolsVersion
        id("space.kscience.gradle.jvm") version toolsVersion
    }
}

dependencyResolutionManagement {

    val toolsVersion: String by extra

    repositories {
        mavenLocal()
        maven("https://repo.kotlin.link")
        mavenCentral()
    }

    versionCatalogs {
        create("spclibs") {
            from("space.kscience:version-catalog:$toolsVersion")
        }
    }
}

include(
    ":visionforge-core",
    ":visionforge-compose-html",
    ":visionforge-compose-multiplatform",
    ":visionforge-solid",
    ":visionforge-threejs",
    ":visionforge-threejs:visionforge-threejs-server",
    ":visionforge-gdml",
    ":cern-root-loader",
    ":visionforge-server",
    ":visionforge-plotly",
    ":visionforge-tables",
    ":visionforge-markdown",
    ":demo:solid-showcase",
    ":demo:gdml",
    ":demo:muon-monitor",
    ":demo:sat-demo",
    ":demo:playground",
    ":demo:js-playground",
    ":demo:compose-desktop-demo",
    ":visionforge-jupyter",
    ":visionforge-jupyter:visionforge-jupyter-common",
    ":plotly",
    ":plotly:plotlykt-core",
    ":plotly:plotlykt-jupyter",
    ":plotly:plotlykt-server",
    ":plotly:plotlykt-script",
    ":plotly:examples",
    ":plotly:examples:fx-demo",
    ":plotly:examples:compose-demo",
    ":plotly:examples:js-demo",
    ":plotly:examples:native-demo"
)
